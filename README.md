# teoria-grafos


## Descrição

Esse é o repositório da Disciplina COS242 - Teoria dos Grafos e contém os exercícios propostos e suas respectivas resoluções.

### Alunas Responsáveis:
- Ana Carolina Jaolino Linhares
- Juliana Fernandes Dal Piaz

## Como baixar esse repositório:
```
git clone https://gitlab.com/umjourje/teoria-grafos.git
```

## Relatório no Overleaf
- [Link TP1](https://www.overleaf.com/project/61c5dd69d2d6e0912b87e750)

